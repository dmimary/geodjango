const _ = require('underscore');
const Bb = require('backbone');
const Mn = require('backbone.marionette');
const Tc = require('marionette.templatecache');
const L = require('leaflet');

require('leaflet-osm');
require('leaflet-draw');
require('leaflet.measurecontrol');
require('leaflet-mouse-position');
require('bootstrap');
require('./main.css');

Mn.setRenderer(Tc.default.render);

const App = Mn.Application.extend({
  region: '#app',
  
  initialize(options) {
    console.log('Initialize');
  },
  
  onStart(app, options) {
    Bb.history.start();
  }
});

const app = new App();

const sights = new Bb.Collection([{
    name: 'park',
    title: 'Парк',
    description: 'Студенческий парк ДГТУ<br>им. Л.В. Красниченко'
  }, {
    name: 'dormitory',
    title: 'Общежитие',
    description: 'Общежитие ДГТУ'
  }, {
    name: 'bus_stop',
    title: 'Остановка ДГТУ',
    description: 'автобус: 1, 2a, 5, 22, 25, 33, 42, 45, 49, 65, 65a, 77, 83, 92'+
    ', 92a, 452'
  }, {
    name: 'sports_arena',
    title: 'Манеж ДГТУ',
    description: 'Спортивный легкоатлетический манеж ДГТУ'
  }, {
    name: 'church',
    title: 'Храм',
    description: 'Храм Святой Великомученицы Татианы'
  }, {
    name: 'gas_station',
    title: 'АЗС',
    description: 'Роснефть<br/> ДТ, 92, 95, 95+, 98'
  }
]);

const SightsChildView = Mn.View.extend({
  className: 'sight',
  tagName: 'div',

  template: _.template(
    '<div class="header">' +
      '<%= title %>' +
    '</div>' +
    '<%= description %>'
  ),
  
  events: {
    'click': 'onClick'
  },
  
  onClick() {
    this.triggerMethod('capture:name', this.model.get('name'));
  },
});

const SightsCollectionView = Mn.CollectionView.extend({
  childView: SightsChildView,
  collection: sights,
  childViewEventPrefix: 'childview',
  
  onChildviewCaptureName(name) {
    this.triggerMethod('capture:name', name);
  }
});

const sightsCollectionView = new SightsCollectionView();

const LandingView = Mn.View.extend({
  template: '#template-layout',
  
  events: {
    'click #tomap': 'onClick'
  },
  
  onClick() {
    router.navigate('map', { trigger: true });
  }
});

const MapView = Mn.View.extend({
  id: 'map',

  template: _.template('<div id="sights-control"></div>'),
  
  childViewEventPrefix: 'childview',
  
  regions: {
    sightsControl: '#sights-control'
  },
  
  onDomRefresh() {
    this.map = L.map('map').setView([47.240641, 39.711446], 17);
    
    new L.OSM.Mapnik().addTo(this.map);
    
    L.Control.measureControl().addTo(this.map);
    L.control.mousePosition().addTo(this.map);
    
    const CustomIcon = L.Icon.extend({
      options: {
          iconSize:    [35, 35],
          popupAnchor: [0, -15]
      }
    });
    
    const parkIcon = new CustomIcon({iconUrl: 'icons/park.png'});
    const dormitoryIcon = new CustomIcon({iconUrl: 'icons/bunk-bed.png'});
    const sportsArenaIcon = new CustomIcon({iconUrl: 'icons/dumbbell.png'});
    const churchIcon = new CustomIcon({iconUrl: 'icons/church.png'});
    const gasStationIcon = new CustomIcon({iconUrl: 'icons/gas-station.png'});
    const busStopIcon = L.icon({
      iconUrl: 'icons/bus-stop.png',
      iconSize: [25, 25],
      popupAnchor: [0, -25]
    });

    this.markers = {
      park: L.marker([47.240136, 39.710674], {icon: parkIcon}).addTo(this.map).bindPopup("Студенческий парк ДГТУ"),
      dormitory: L.marker([47.239378, 39.712755], {icon: dormitoryIcon}).addTo(this.map).bindPopup("Общежитие ДГТУ"),
      bus_stop: L.marker([47.238739, 39.713232], {icon: busStopIcon}).addTo(this.map).bindPopup("Остановка ДГТУ"),
      sports_arena: L.marker([47.241041, 39.710062], {icon: sportsArenaIcon}).addTo(this.map).bindPopup("Манеж ДГТУ"),
      church: L.marker([47.239318, 39.711066], {icon: churchIcon}).addTo(this.map).bindPopup("Храм св. Татианы"),
      gas_station: L.marker([47.242353, 39.712869], {icon: gasStationIcon}).addTo(this.map).bindPopup("АЗС Роснефть")
    };
  },
  
  onRender() {
    this.showChildView('sightsControl', sightsCollectionView);
  },
  
  onChildviewCaptureName(name) {
    const marker = this.markers[name];
    
    marker.openPopup();
    this.map.flyTo(marker.getLatLng(), 19);
  }
});

const landingView = new LandingView;

const mapView = new MapView();

const Router = Bb.Router.extend({
  routes: {
    '': 'LandingPage',
    'map': 'MapPage'
  },
  LandingPage() {
    app.showView(landingView);
  },
	MapPage() {
    app.showView(mapView);
  }
});

const router = new Router();

document.addEventListener("DOMContentLoaded", () => {
  app.start();
});