from django.contrib.gis.db import models as gismodels
from django.db import models


class Markers(models.Model):
    markerId = models.IntegerField(primary_key=True)
    name = models.CharField(max_length=256)

    geom = gismodels.PointField()
    objects = gismodels.GeometryField()

    def __str__(self):
        return self.name


class WorldBorder(gismodels.Model):
    # Regular Django fields corresponding to the attributes in the
    # world borders shapefile.
    name = gismodels.CharField(max_length=50)
    area = gismodels.IntegerField()
    pop2005 = gismodels.IntegerField('Population 2005')
    fips = gismodels.CharField('FIPS Code', max_length=2)
    iso2 = gismodels.CharField('2 Digit ISO', max_length=2)
    iso3 = gismodels.CharField('3 Digit ISO', max_length=3)
    un = gismodels.IntegerField('United Nations Code')
    region = gismodels.IntegerField('Region Code')
    subregion = gismodels.IntegerField('Sub-Region Code')
    lon = gismodels.FloatField()
    lat = gismodels.FloatField()

    # GeoDjango-specific: a geometry field (MultiPolygonField)
    mpoly = gismodels.MultiPointField()

    def __str__(self):
        return  self.name
